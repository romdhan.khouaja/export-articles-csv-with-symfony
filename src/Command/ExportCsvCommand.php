<?php

namespace App\Command;
 
use App\Entity\Article;
use ArrayIterator;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption; 
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface; 
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;  
use League\Csv\Writer; 
use League\Csv\CannotInsertRecord;
use RuntimeException;
use SplFileObject;
use SplTempFileObject;

class ExportCsvCommand extends Command  
{
    protected static $defaultName = 'cvs:export:from:datatabase';
    protected $count_error_duplicate = 0 ;
    protected $count_error_file_duplicate = 0 ;
    protected $count_success_file = 0 ;
    protected $count_success = 0 ;
    private $params;
    private $stateNiveau ;
    private $stateMatiere;
    private $em;

    /**
     * CsvImportCommand constructor.
     *
     * @param EntityManagerInterface $em
     *
     * @throws \Symfony\Component\Console\Exception\LogicException
     */
    /**
     * CsvImportCommand constructor.
     *
     * @param EntityManagerInterface $em
     * @param ParameterBagInterface $params
     */
    public function __construct(EntityManagerInterface $em,ParameterBagInterface $params)
    { 
        parent::__construct();
        $this->em = $em;
        $this->params = $params;
        $this->count_error_duplicate = 0;
        $this->count_success = 0; 
        $this->count_error_file_duplicate = 0;
        $this->count_success_file = 0 ;
    }

    protected function configure()
    {
        $this
            ->setDescription('Creates a new export for csv file.')
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument file destination') 
            ->addOption('option1', null, InputOption::VALUE_OPTIONAL , 'Option description')
        ;
    }
    // fonction permettant de traiter le chemin dans windows rendre les / en \ 
    private function getCheminFichier(string $chaine){
        $app_path_csv_documents = $this->params->get('app.path.csv_documents');
        $kernel_project_dir = $this->params->get('kernel.project_dir');
        $resulat_chemin = $kernel_project_dir.'/public'.$app_path_csv_documents.'/'.$chaine ;
        // $dd = str_replace('\’', '\'', "String’s Title");
        // $chemin = str_replace('/', '\\', $resulat_chemin);
        return $resulat_chemin;
    }
    
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // $io = new SymfonyStyle($input, $output);
        // $io->title('Importation des données ...<br>');
        $arg1 = $input->getArgument('arg1');
        // dd($arg1);
        if ($arg1) {
            //  $io->note(sprintf('...Fichier nommé : %s <br>', $arg1));
            $chemin = $this->getCheminFichier($arg1);
        } 
        if ($input->getOption('option1')) {
            // aucune option à placer  
        }
        $articles = $this->em->getRepository(Article::class)->findAll();  
       
        /*try {
            $csv = Writer::createFromPath( $chemin , 'w+'); 
            $csv->insertAll($records);
        } catch (CannotInsertRecord $e) {
            $e->getRecords(); //returns [1, 2, 3]
        }
        try{
            $csv->insertAll(new ArrayIterator($articles));
        }*/
        $tab = array();
        foreach($articles as $article){
            $tab[]=array($article->getId(),$article->getName(),$article->getDescription());
        }
        //dd($tab);
        try {
            $csv = Writer::createFromFileObject(new SplFileObject($chemin, 'w'));
            $csv->insertOne(["id","name","description"]);
            
            $csv->insertAll($tab);
        } catch (Exception | RuntimeException $e) {
            echo $e->getMessage(), PHP_EOL;
        }
        
        return Command::SUCCESS;
    }
        
}
