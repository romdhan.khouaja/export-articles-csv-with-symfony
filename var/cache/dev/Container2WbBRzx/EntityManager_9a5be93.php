<?php

namespace Container2WbBRzx;
include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/src/Persistence/ObjectManager.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolder1d19d = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializerce354 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicPropertiesc2eb8 = [
        
    ];

    public function getConnection()
    {
        $this->initializerce354 && ($this->initializerce354->__invoke($valueHolder1d19d, $this, 'getConnection', array(), $this->initializerce354) || 1) && $this->valueHolder1d19d = $valueHolder1d19d;

        return $this->valueHolder1d19d->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializerce354 && ($this->initializerce354->__invoke($valueHolder1d19d, $this, 'getMetadataFactory', array(), $this->initializerce354) || 1) && $this->valueHolder1d19d = $valueHolder1d19d;

        return $this->valueHolder1d19d->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializerce354 && ($this->initializerce354->__invoke($valueHolder1d19d, $this, 'getExpressionBuilder', array(), $this->initializerce354) || 1) && $this->valueHolder1d19d = $valueHolder1d19d;

        return $this->valueHolder1d19d->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializerce354 && ($this->initializerce354->__invoke($valueHolder1d19d, $this, 'beginTransaction', array(), $this->initializerce354) || 1) && $this->valueHolder1d19d = $valueHolder1d19d;

        return $this->valueHolder1d19d->beginTransaction();
    }

    public function getCache()
    {
        $this->initializerce354 && ($this->initializerce354->__invoke($valueHolder1d19d, $this, 'getCache', array(), $this->initializerce354) || 1) && $this->valueHolder1d19d = $valueHolder1d19d;

        return $this->valueHolder1d19d->getCache();
    }

    public function transactional($func)
    {
        $this->initializerce354 && ($this->initializerce354->__invoke($valueHolder1d19d, $this, 'transactional', array('func' => $func), $this->initializerce354) || 1) && $this->valueHolder1d19d = $valueHolder1d19d;

        return $this->valueHolder1d19d->transactional($func);
    }

    public function wrapInTransaction(callable $func)
    {
        $this->initializerce354 && ($this->initializerce354->__invoke($valueHolder1d19d, $this, 'wrapInTransaction', array('func' => $func), $this->initializerce354) || 1) && $this->valueHolder1d19d = $valueHolder1d19d;

        return $this->valueHolder1d19d->wrapInTransaction($func);
    }

    public function commit()
    {
        $this->initializerce354 && ($this->initializerce354->__invoke($valueHolder1d19d, $this, 'commit', array(), $this->initializerce354) || 1) && $this->valueHolder1d19d = $valueHolder1d19d;

        return $this->valueHolder1d19d->commit();
    }

    public function rollback()
    {
        $this->initializerce354 && ($this->initializerce354->__invoke($valueHolder1d19d, $this, 'rollback', array(), $this->initializerce354) || 1) && $this->valueHolder1d19d = $valueHolder1d19d;

        return $this->valueHolder1d19d->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializerce354 && ($this->initializerce354->__invoke($valueHolder1d19d, $this, 'getClassMetadata', array('className' => $className), $this->initializerce354) || 1) && $this->valueHolder1d19d = $valueHolder1d19d;

        return $this->valueHolder1d19d->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializerce354 && ($this->initializerce354->__invoke($valueHolder1d19d, $this, 'createQuery', array('dql' => $dql), $this->initializerce354) || 1) && $this->valueHolder1d19d = $valueHolder1d19d;

        return $this->valueHolder1d19d->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializerce354 && ($this->initializerce354->__invoke($valueHolder1d19d, $this, 'createNamedQuery', array('name' => $name), $this->initializerce354) || 1) && $this->valueHolder1d19d = $valueHolder1d19d;

        return $this->valueHolder1d19d->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializerce354 && ($this->initializerce354->__invoke($valueHolder1d19d, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializerce354) || 1) && $this->valueHolder1d19d = $valueHolder1d19d;

        return $this->valueHolder1d19d->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializerce354 && ($this->initializerce354->__invoke($valueHolder1d19d, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializerce354) || 1) && $this->valueHolder1d19d = $valueHolder1d19d;

        return $this->valueHolder1d19d->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializerce354 && ($this->initializerce354->__invoke($valueHolder1d19d, $this, 'createQueryBuilder', array(), $this->initializerce354) || 1) && $this->valueHolder1d19d = $valueHolder1d19d;

        return $this->valueHolder1d19d->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializerce354 && ($this->initializerce354->__invoke($valueHolder1d19d, $this, 'flush', array('entity' => $entity), $this->initializerce354) || 1) && $this->valueHolder1d19d = $valueHolder1d19d;

        return $this->valueHolder1d19d->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializerce354 && ($this->initializerce354->__invoke($valueHolder1d19d, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializerce354) || 1) && $this->valueHolder1d19d = $valueHolder1d19d;

        return $this->valueHolder1d19d->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializerce354 && ($this->initializerce354->__invoke($valueHolder1d19d, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializerce354) || 1) && $this->valueHolder1d19d = $valueHolder1d19d;

        return $this->valueHolder1d19d->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializerce354 && ($this->initializerce354->__invoke($valueHolder1d19d, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializerce354) || 1) && $this->valueHolder1d19d = $valueHolder1d19d;

        return $this->valueHolder1d19d->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializerce354 && ($this->initializerce354->__invoke($valueHolder1d19d, $this, 'clear', array('entityName' => $entityName), $this->initializerce354) || 1) && $this->valueHolder1d19d = $valueHolder1d19d;

        return $this->valueHolder1d19d->clear($entityName);
    }

    public function close()
    {
        $this->initializerce354 && ($this->initializerce354->__invoke($valueHolder1d19d, $this, 'close', array(), $this->initializerce354) || 1) && $this->valueHolder1d19d = $valueHolder1d19d;

        return $this->valueHolder1d19d->close();
    }

    public function persist($entity)
    {
        $this->initializerce354 && ($this->initializerce354->__invoke($valueHolder1d19d, $this, 'persist', array('entity' => $entity), $this->initializerce354) || 1) && $this->valueHolder1d19d = $valueHolder1d19d;

        return $this->valueHolder1d19d->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializerce354 && ($this->initializerce354->__invoke($valueHolder1d19d, $this, 'remove', array('entity' => $entity), $this->initializerce354) || 1) && $this->valueHolder1d19d = $valueHolder1d19d;

        return $this->valueHolder1d19d->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializerce354 && ($this->initializerce354->__invoke($valueHolder1d19d, $this, 'refresh', array('entity' => $entity), $this->initializerce354) || 1) && $this->valueHolder1d19d = $valueHolder1d19d;

        return $this->valueHolder1d19d->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializerce354 && ($this->initializerce354->__invoke($valueHolder1d19d, $this, 'detach', array('entity' => $entity), $this->initializerce354) || 1) && $this->valueHolder1d19d = $valueHolder1d19d;

        return $this->valueHolder1d19d->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializerce354 && ($this->initializerce354->__invoke($valueHolder1d19d, $this, 'merge', array('entity' => $entity), $this->initializerce354) || 1) && $this->valueHolder1d19d = $valueHolder1d19d;

        return $this->valueHolder1d19d->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializerce354 && ($this->initializerce354->__invoke($valueHolder1d19d, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializerce354) || 1) && $this->valueHolder1d19d = $valueHolder1d19d;

        return $this->valueHolder1d19d->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializerce354 && ($this->initializerce354->__invoke($valueHolder1d19d, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializerce354) || 1) && $this->valueHolder1d19d = $valueHolder1d19d;

        return $this->valueHolder1d19d->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializerce354 && ($this->initializerce354->__invoke($valueHolder1d19d, $this, 'getRepository', array('entityName' => $entityName), $this->initializerce354) || 1) && $this->valueHolder1d19d = $valueHolder1d19d;

        return $this->valueHolder1d19d->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializerce354 && ($this->initializerce354->__invoke($valueHolder1d19d, $this, 'contains', array('entity' => $entity), $this->initializerce354) || 1) && $this->valueHolder1d19d = $valueHolder1d19d;

        return $this->valueHolder1d19d->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializerce354 && ($this->initializerce354->__invoke($valueHolder1d19d, $this, 'getEventManager', array(), $this->initializerce354) || 1) && $this->valueHolder1d19d = $valueHolder1d19d;

        return $this->valueHolder1d19d->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializerce354 && ($this->initializerce354->__invoke($valueHolder1d19d, $this, 'getConfiguration', array(), $this->initializerce354) || 1) && $this->valueHolder1d19d = $valueHolder1d19d;

        return $this->valueHolder1d19d->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializerce354 && ($this->initializerce354->__invoke($valueHolder1d19d, $this, 'isOpen', array(), $this->initializerce354) || 1) && $this->valueHolder1d19d = $valueHolder1d19d;

        return $this->valueHolder1d19d->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializerce354 && ($this->initializerce354->__invoke($valueHolder1d19d, $this, 'getUnitOfWork', array(), $this->initializerce354) || 1) && $this->valueHolder1d19d = $valueHolder1d19d;

        return $this->valueHolder1d19d->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializerce354 && ($this->initializerce354->__invoke($valueHolder1d19d, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializerce354) || 1) && $this->valueHolder1d19d = $valueHolder1d19d;

        return $this->valueHolder1d19d->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializerce354 && ($this->initializerce354->__invoke($valueHolder1d19d, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializerce354) || 1) && $this->valueHolder1d19d = $valueHolder1d19d;

        return $this->valueHolder1d19d->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializerce354 && ($this->initializerce354->__invoke($valueHolder1d19d, $this, 'getProxyFactory', array(), $this->initializerce354) || 1) && $this->valueHolder1d19d = $valueHolder1d19d;

        return $this->valueHolder1d19d->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializerce354 && ($this->initializerce354->__invoke($valueHolder1d19d, $this, 'initializeObject', array('obj' => $obj), $this->initializerce354) || 1) && $this->valueHolder1d19d = $valueHolder1d19d;

        return $this->valueHolder1d19d->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializerce354 && ($this->initializerce354->__invoke($valueHolder1d19d, $this, 'getFilters', array(), $this->initializerce354) || 1) && $this->valueHolder1d19d = $valueHolder1d19d;

        return $this->valueHolder1d19d->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializerce354 && ($this->initializerce354->__invoke($valueHolder1d19d, $this, 'isFiltersStateClean', array(), $this->initializerce354) || 1) && $this->valueHolder1d19d = $valueHolder1d19d;

        return $this->valueHolder1d19d->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializerce354 && ($this->initializerce354->__invoke($valueHolder1d19d, $this, 'hasFilters', array(), $this->initializerce354) || 1) && $this->valueHolder1d19d = $valueHolder1d19d;

        return $this->valueHolder1d19d->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializerce354 = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolder1d19d) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolder1d19d = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolder1d19d->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializerce354 && ($this->initializerce354->__invoke($valueHolder1d19d, $this, '__get', ['name' => $name], $this->initializerce354) || 1) && $this->valueHolder1d19d = $valueHolder1d19d;

        if (isset(self::$publicPropertiesc2eb8[$name])) {
            return $this->valueHolder1d19d->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder1d19d;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder1d19d;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializerce354 && ($this->initializerce354->__invoke($valueHolder1d19d, $this, '__set', array('name' => $name, 'value' => $value), $this->initializerce354) || 1) && $this->valueHolder1d19d = $valueHolder1d19d;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder1d19d;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder1d19d;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializerce354 && ($this->initializerce354->__invoke($valueHolder1d19d, $this, '__isset', array('name' => $name), $this->initializerce354) || 1) && $this->valueHolder1d19d = $valueHolder1d19d;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder1d19d;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolder1d19d;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializerce354 && ($this->initializerce354->__invoke($valueHolder1d19d, $this, '__unset', array('name' => $name), $this->initializerce354) || 1) && $this->valueHolder1d19d = $valueHolder1d19d;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder1d19d;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolder1d19d;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializerce354 && ($this->initializerce354->__invoke($valueHolder1d19d, $this, '__clone', array(), $this->initializerce354) || 1) && $this->valueHolder1d19d = $valueHolder1d19d;

        $this->valueHolder1d19d = clone $this->valueHolder1d19d;
    }

    public function __sleep()
    {
        $this->initializerce354 && ($this->initializerce354->__invoke($valueHolder1d19d, $this, '__sleep', array(), $this->initializerce354) || 1) && $this->valueHolder1d19d = $valueHolder1d19d;

        return array('valueHolder1d19d');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializerce354 = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializerce354;
    }

    public function initializeProxy() : bool
    {
        return $this->initializerce354 && ($this->initializerce354->__invoke($valueHolder1d19d, $this, 'initializeProxy', array(), $this->initializerce354) || 1) && $this->valueHolder1d19d = $valueHolder1d19d;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder1d19d;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder1d19d;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
